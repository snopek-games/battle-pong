extends Control

var player1_score : int = 0
var player2_score : int = 0

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.

remotesync func increment_score(player1 : int, player2: int):
	if get_tree().get_rpc_sender_id() != 1:
		return

	player1_score += player1
	player2_score += player2

	$Player1Score.text = str(player1_score)
	$Player2Score.text = str(player2_score)

func show_message(message):
	$Message.visible = true
	$Message.text = message

func hide_message():
	$Message.visible = false
