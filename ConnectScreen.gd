extends PanelContainer

signal connect(host, port)
signal serve(port)

func _on_ServeButton_pressed() -> void:
	emit_signal("serve", int($'%PortField'.text))

func _on_ConnectButton_pressed() -> void:
	emit_signal("connect", $'%HostField'.text, int($'%PortField'.text))
